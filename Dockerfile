FROM php:8.1-apache

RUN a2enmod rewrite

RUN docker-php-ext-install pdo_mysql 

RUN cat /etc/os-release

RUN apt-get update

RUN apt-get install -y libzip-dev 

RUN apt-get install -y zip
RUN apt-get install -y unzip 
RUN apt-get install -y openssl 
RUN apt-get install -y libcurl4-openssl-dev 
RUN apt-get install -y wget 
RUN apt-get install -y zlib1g-dev 
RUN apt-get install -y libxml2-dev 
RUN apt-get install -y git 
RUN apt-get install -y libssh-dev 
RUN apt-get install -y vim
RUN apt-get install -y libpng-dev
RUN docker-php-ext-install zip 
RUN docker-php-ext-install bcmath 
RUN docker-php-ext-install sockets 
RUN docker-php-ext-install soap

ENV TZ=Asia/Tehran
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer 
RUN rm -rf composer-setup.php && composer config --global repos.packagist composer https://packagist.org
